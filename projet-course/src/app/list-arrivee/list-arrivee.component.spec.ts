import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListArriveeComponent } from './list-arrivee.component';

describe('ListArriveeComponent', () => {
  let component: ListArriveeComponent;
  let fixture: ComponentFixture<ListArriveeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListArriveeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListArriveeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
