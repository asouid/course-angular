import {Component, OnInit} from '@angular/core';
import {CoureurService} from '../services/coureur.service';
import {HttpClient} from '@angular/common/http';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-list-arrivee',
  templateUrl: './list-arrivee.component.html',
  styles: []
})
export class ListArriveeComponent implements OnInit {
  //coureurs: any [];
  coureurs: Array<any>;

  constructor(private coureurService: CoureurService,
              private httpClient: HttpClient) {
  }

  ngOnInit() {/*
    const url = '//localhost:8080/arrivee?dossard=3333';
    this.httpClient.get<any>(url)
      .subscribe(
        (response) => {
          console.log(response);
          this.coureurs = response;
          console.log('---------------------------');
          console.log(this.coureurs);
        },
        (error) => {
          console.log('error !!!!...' + error);
        });*/
    /*
        this.coureurService.getAll().subscribe(
          (response) => {
            console.log(response);
            this.coureurs = response;
          },
          (error) => {
            console.log('Impossible de se connecter!!...' + error);
          }
        );*/
  }

  onSubmit(form: NgForm) {
    const nDossard = form.value['numDossard'];
    this.coureurService.getAll(nDossard).subscribe(
      (response) => {
        console.log(response);
        this.coureurs = response;
      },
      (error) => {
        console.log('Impossible de se connecter!!...' + error);
      }
    );
  }

}
