import {Component, OnInit} from '@angular/core';
import {CoureurService} from '../services/coureur.service';

@Component({
  selector: 'app-depart',
  templateUrl: './depart.component.html',
  styles: []
})
export class DepartComponent implements OnInit {

  constructor(private coureurService: CoureurService) {
  }

  ngOnInit() {
  }

  onDepart() {
    this.coureurService.start().subscribe(
      () => {
        console.log('Depart OK !!');
      },
      () => {
        console.log('Erreur de connexion !!!');
      }
    );
  }
}
