import { TestBed } from '@angular/core/testing';

import { CoureurService } from './coureur.service';

describe('CoureurService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CoureurService = TestBed.get(CoureurService);
    expect(service).toBeTruthy();
  });
});
