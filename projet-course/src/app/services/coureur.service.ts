import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CoureurService {

  constructor(private httpClient: HttpClient) {
  }

  getAll(num: number): Observable<any> {
    return this.httpClient.get('//localhost:8080/course/arrivee?dossard=' + num);
  }

  start(): Observable<any> {
    return this.httpClient.get('//localhost:8080/course/depart');
  }
}
