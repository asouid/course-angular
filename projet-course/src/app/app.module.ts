import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ListArriveeComponent} from './list-arrivee/list-arrivee.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import { DepartComponent } from './depart/depart.component';
import {FormsModule} from '@angular/forms';

const appRoutes: Routes = [
  {path: 'home', component: DepartComponent},
  {path: 'arrivee', component: ListArriveeComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ListArriveeComponent,
    DepartComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
